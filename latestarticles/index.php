<?php
/*
Plugin Name: Recent Posts Center Mersenne
Plugin URI: https://www.centre-mersenne.org/
Description: This plugin allows you to display the 3 recent articles of the mersenne center
Version: 1.0.0
Author: Mathdoc
Author URI: http://www.mathdoc.fr/
*/

function articles_recents_widget_init()
{
    register_widget('articles_recents_mersenne');
}

add_action('widgets_init', 'articles_recents_widget_init');

class articles_recents_mersenne extends WP_Widget
{
    /*
     * Constructeur de la classe articles_recents qui hérite de la classe WP_Widget
     */
    public function __construct()
    {
        parent::__construct(
        // L'id du widget
            'articles_recents_mersenne',

            // Le nom du widget qui va apparaitre dans l'interface utilisateur
            __('Articles Récents Mersenne', 'articles_recents_domain'),

            // Description du widget
            array('description' => __('Affiche les articles récents qui ont été publié sur le site du Centre Mersenne', 'articles_recents_domain'),)
        );
    }

    /*
     * La fonction permet de définir ce que va devoir faire le widget
     *
     *
     * @param array $args Il s'agit d'un tableau de valeurs contenant plein de choses sur la page. Ces éléments sont récupérés en fonction de l'emplacement du widget
     * @param array instance Il s'agit d'un tableau de valeurs vide.
     */

    public function widget($args, $instance)
    {
        extract($args);
        $datas_api = json_decode(wp_remote_retrieve_body(wp_remote_get('https://jep.centre-mersenne.org/api-articles-recents/')));
        echo $before_widget;
        echo '<div class="container_articles_recents">';

        foreach ($datas_api as $current_element) {
            for ($i = 0; $i < 3; $i++) {
                echo '<div class="articles_recents">';
                echo '<h3 class="title_article">' . $current_element[$i]->title . '</h3>';
                echo '<h4 class="authors">' . $current_element[$i]->authors . '</h4>';
                echo '<p class="collection">' . $current_element[$i]->collection . '</p>';
                echo '<p class="pages">' . substr($current_element[$i]->citation_source,1) . '</p>';
		if(ICL_LANGUAGE_CODE=='en'){
                    echo '<a class="url_article lire_article" href="' . $current_element[$i]->url . '">read the article</a>';
		} else {
                    echo '<a class="url_article lire_article" href="' . $current_element[$i]->url . '">lire l\'article</a>';
		}
                echo '</div>';
            }
        }
        echo '</div>';
        echo $after_widget;
    }
}
