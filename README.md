# Mersenne Latest Articles

Wordpress pluging to display Mersenne recently published articles

## Summary

1. [How to install WordPress ?](#worpress)
2. [How to use the plugin ?](#plugin)

----

### **How to install WordPress ?** <a name="wordpress"></a>

To install WordPress please go to the web page : ```https://fr.wordpress.org/download/ ```
Once downloaded, please launch a local PHP server using this command ```php -S localhost:8080```
Go to the link : ```http://localhost:8080/wp-admin/``` and follow the instructions

----

### **How to use the plugin ?** <a name="plugin"></a>

To be able to use the plugin, just copy paste the ```"latestarticles"``` directory in ```wp-content/plugins```. To apply the style for the plugin, please copy paste the contents of the ```style.css``` file which is located in ```wp-content/plugins/latestarticles``` in the ```style.css``` file of your theme this file is located in ```wp-content/themes/name_of_theme/style.css```. Once the contents of the file copy paste you can delete this file. You are now ready to use the plugin for this please go to the following link : ```http://localhost:8080/wp-admin/plugins.php?plugin_status=all&paged=1&s```. You can now click on the activate link of the ```Recent Posts Center Mersenne``` plugin
